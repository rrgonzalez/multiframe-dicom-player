﻿using System;
using System.Windows.Media;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DirtyRect
{
    public class DifferenceImageMask
    {
        #region Fields

        private readonly int _width;
        private readonly int _height;
        private bool[,] _differenceMask;
        private int[,] _accumulatedDiff;

        #endregion

        #region Constructors

        public DifferenceImageMask(ImagePtr imageA, ImagePtr imageB)
        {
            this._width = imageA.Width;
            this._height = imageA.Height;

            this.DifferenceMask = new bool[Height, Width];

            #region Calculate the difference image  mask
            if (imageA.Format == PixelFormats.Bgr32)
            {
                DifferenceMask = calculateDifferenceImageMask32(imageA, imageB);
            }
            else if (imageA.Format == PixelFormats.Bgr24 || imageA.Format == PixelFormats.Rgb24)
            {
                DifferenceMask = calculateDifferenceImageMask24(imageA, imageB);
            }
            else if (imageA.Format == PixelFormats.Gray16)
            {
                DifferenceMask = calculateDifferenceImageMask16(imageA, imageB);
            }
            else if (imageA.Format == PixelFormats.Gray8)
            {
                DifferenceMask = calculateDifferenceImageMask8(imageA, imageB);
            }
            #endregion

            #region Calculate the Accumulated Sum of Difference
            _accumulatedDiff = new int[Height+1, Width+1];

            // Fill the first col and row with 0
            for (int row = 0; row < Height + 1; ++row)
                _accumulatedDiff[row, 0] = 0;

            for (int col = 0; col < Width + 1; ++col)
                _accumulatedDiff[0, col] = 0;

            for (int row = 1; row < Height+1; ++row)
            {
                for (int col = 1; col < Width+1; ++col)
                {
                    _accumulatedDiff[row, col] = _accumulatedDiff[row, col - 1] + _accumulatedDiff[row - 1, col];
                    if (DifferenceMask[row-1, col-1])
                        ++_accumulatedDiff[row, col];
                }
            }
            #endregion
        }

        #endregion

        #region Properties

        public int Width => _width;

        public int Height => _height;

        public bool[,] DifferenceMask { get => _differenceMask; set => _differenceMask = value; }

        #endregion

        #region Methods

        public double getDifferenceRatio(Int32Rect rect)
        {
            double totalPixels = rect.Width * rect.Height;
            double countDifferentPixels = amountDifferentPixels(rect);

            return (countDifferentPixels / totalPixels);
        }

        #endregion

        #region Helper Methods
        private unsafe bool[,] calculateDifferenceImageMask32(ImagePtr imageA, ImagePtr imageB)
        {
            bool[,] result = new bool[imageA.Height, imageA.Width];
            bool differ;

            Bgr32* dataA = (Bgr32*)imageA.Data;
            Bgr32* dataB = (Bgr32*)imageB.Data;

            for (int row = 0; row < Height; row++)
            {
                for (int col = 0; col < Width; col++)
                {
                    differ = (dataA[row * Width + col].R != dataB[row * Width + col].R);
                    differ = differ || (dataA[row * Width + col].G != dataB[row * Width + col].G);
                    differ = differ || (dataA[row * Width + col].B != dataB[row * Width + col].B);
                    differ = differ || (dataA[row * Width + col].S != dataB[row * Width + col].S);

                    result[row, col] = differ;
                }
            }

            return result;
        }

        private unsafe bool[,] calculateDifferenceImageMask24(ImagePtr imageA, ImagePtr imageB)
        {
            bool[,] result = new bool[imageA.Height, imageA.Width];
            bool differ;

            Bgr24* dataA = (Bgr24*)imageA.Data;
            Bgr24* dataB = (Bgr24*)imageB.Data;

            for (int row = 0; row < Height; row++)
            {
                for (int col = 0; col < Width; col++)
                {
                    differ = (dataA[row * Width + col].R != dataB[row * Width + col].R);
                    differ = differ || (dataA[row * Width + col].G != dataB[row * Width + col].G);
                    differ = differ || (dataA[row * Width + col].B != dataB[row * Width + col].B);

                    result[row, col] = differ;
                }
            }

            return result;
        }

        private unsafe bool[,] calculateDifferenceImageMask16(ImagePtr imageA, ImagePtr imageB)
        {
            bool[,] result = new bool[imageA.Height, imageA.Width];
            bool differ;

            Int16* dataA = (Int16*)imageA.Data;
            Int16* dataB = (Int16*)imageB.Data;

            for (int row = 0; row < Height; row++)
            {
                for (int col = 0; col < Width; col++)
                {
                    differ = (dataA[row * Width + col] != dataB[row * Width + col]);

                    result[row, col] = differ;
                }
            }

            return result;
        }

        private unsafe bool[,] calculateDifferenceImageMask8(ImagePtr imageA, ImagePtr imageB)
        {
            bool[,] result = new bool[imageA.Height, imageA.Width];
            bool differ;

            byte* dataA = (byte*)imageA.Data;
            byte* dataB = (byte*)imageB.Data;

            for (int row = 0; row < Height; row++)
            {
                for (int col = 0; col < Width; col++)
                {
                    differ = (dataA[row * Width + col] != dataB[row * Width + col]);

                    result[row, col] = differ;
                }
            }

            return result;
        }

        private int amountDifferentPixels(Int32Rect rect)
        {
            int count = _accumulatedDiff[rect.X + rect.Width, rect.Y + rect.Height];
            count -= _accumulatedDiff[rect.X - 1, rect.Y + rect.Height];
            count -= _accumulatedDiff[rect.X + rect.Width, rect.Y - 1];
            count += _accumulatedDiff[rect.X - 1, rect.Y - 1];

            return count;
        }
        #endregion
    }
}
