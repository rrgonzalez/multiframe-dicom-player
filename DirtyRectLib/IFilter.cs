﻿using System;
using System.Windows.Media;
//using Calib.Imaging;

namespace DirtyRect
{
    public interface IFilter
    {
        ImagePtr Apply(ImagePtr pixelData);
    }


}
