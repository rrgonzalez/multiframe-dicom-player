﻿using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace DirtyRect
{
    public static class DirtyRectLib
    {
        public static List<List<Int32Rect>> getDirtyRect(List<ImagePtr> serie)
        {
            List<List<Int32Rect>> serieDirtyRect = new List<List<Int32Rect>>(serie.Count - 1);
            Parallel.For(0, serie.Count - 2, (i) =>
            {
                serieDirtyRect.Add(getDirtyRect(serie[i], serie[i + 1]));
            });

            return serieDirtyRect;
        }

        public static List<Int32Rect> getDirtyRect(ImagePtr imageA, ImagePtr imageB)
        {
            DifferenceImageMask differenceImage = new DifferenceImageMask(imageA, imageB);

            List<Int32Rect> dirtyRects = DFSEnclosingDirtyRect.findEnclosingDirtyRects(differenceImage.DifferenceMask, imageA.Width, imageA.Height);
            return dirtyRects;

            // Split rectangles in 4 parts and evaluate if is good to continue processing 

            //double[] diffRatios = new double[dirtyRects.Count];
            //for (int i = 0; i < dirtyRects.Count; ++i)
            //{
            //    diffRatios[i] = differenceImage.getDifferenceRatio(dirtyRects[i]);
            //}

            //List<Int32Rect> result = new List<Int32Rect>(dirtyRects.Count);
            //foreach (Int32Rect rect in dirtyRects)
            //{
            //    result.AddRange(RectangleDecomposer.decompose(rect, differenceImage));
            //}

            //return result;
        }
    }
}
