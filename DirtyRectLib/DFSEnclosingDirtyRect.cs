﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DirtyRect
{
    public static class DFSEnclosingDirtyRect
    {

        #region Fields

        private static readonly int[] dx = { -1, 1, 0, 0 };
        private static readonly int[] dy = { 0, 0, -1, 1 };

        #endregion

        #region Constructors

        #endregion

        #region Properties

        #endregion

        #region Methods

        public static List<Int32Rect> findEnclosingDirtyRects(bool[,] diffImage, int width, int height)
        {
            //System.Console.WriteLine("Entering DFS, Image Dimensions: WIDTH=" + width + " / HEIGHT=" + height);

            List<Int32Rect> dirtyRects = new List<Int32Rect>();
            bool[,] visited = new bool[height, width];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    visited[y, x] = false;
                }
            }

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (diffImage[y, x] && !visited[y, x])
                        {
                            Point start = new Point(y, x);
                            dfs(start, diffImage, width, height, visited, dirtyRects);
                        }
                }
            }

            return dirtyRects;
        }

        public static void dfs(Point start, bool[,] matrix, int width, int height, bool[,] visited, List<Int32Rect> dirtyRects)
        {
            Point topLeft = new Point(2*height, 2*width);
            Point rightBottom = new Point(-1, -1);
            Stack<Point> stack = new Stack<Point>();
            stack.Push(start);

            while (stack.Count > 0)
            {
                Point v = stack.Pop();
                visited[(int)v.X, (int)v.Y] = true;

                // Check if this point enlarges the boundaries
                if (topLeft.X > v.X)
                    topLeft.X = v.X;
                if (topLeft.Y > v.Y)
                    topLeft.Y = v.Y;

                if (rightBottom.X < v.X)
                    rightBottom.X = v.X;
                if (rightBottom.Y < v.Y)
                    rightBottom.Y = v.Y;

                int row, col;
                for (int i = 0; i < dx.Length; ++i)
                {
                    row = (int)v.X + dx[i];
                    col = (int)v.Y + dy[i];
                    if (inside(new Point(row, col), width, height) && matrix[row, col] && !visited[row, col])
                        stack.Push(new Point(row, col));
                }
            }

            height = (int)(rightBottom.X - topLeft.X) + 1;
            width = (int)(rightBottom.Y - topLeft.Y) + 1;

            dirtyRects.Add(new Int32Rect((int)topLeft.Y, (int)topLeft.X, width, height));
        }

        #endregion

        #region Helper Methods

        private static bool inside(Point p, int width, int height)
        {
            return (p.X >= 0 && p.X < height &&
                    p.Y >= 0 && p.Y < width);
        }

        #endregion
    }
}