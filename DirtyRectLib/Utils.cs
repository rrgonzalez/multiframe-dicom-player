﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.IO;

namespace DirtyRect
{
    class Utils 
    {
        private Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }

        public static ImagePtr calculateDifferenceImageMask(ImagePtr imageA, ImagePtr imageB)
        {
            ImagePtr diffImg = new ImagePtr(imageA.Width, imageA.Height, imageA.Format);
           
            if (diffImg.Format == PixelFormats.Bgr32)
            {
                diffImg = calculateDifferenceImage32(imageA, imageB);
            }
            else if (diffImg.Format == PixelFormats.Bgr24 || diffImg.Format == PixelFormats.Rgb24)
            {
                diffImg = calculateDifferenceImage24(imageA, imageB);
            }
            else if (diffImg.Format == PixelFormats.Gray16)
            {
                diffImg = calculateDifferenceImage16(imageA, imageB);
            }
            else if (diffImg.Format == PixelFormats.Gray8)
            {
                diffImg = calculateDifferenceImage8(imageA, imageB);
            }

            return diffImg;
        }

        private static unsafe ImagePtr calculateDifferenceImage32(ImagePtr imageA, ImagePtr imageB)
        {
            Bgr32* pixelDataA = (Bgr32*)imageA.Data;
            Bgr32* pixelDataB = (Bgr32*)imageB.Data;

            int width = imageA.Width;
            int height = imageA.Height;

            throw new NotImplementedException();
        }

        private static ImagePtr calculateDifferenceImage24(ImagePtr imageA, ImagePtr imageB)
        {
            throw new NotImplementedException();
        }

        private static ImagePtr calculateDifferenceImage16(ImagePtr imageA, ImagePtr imageB)
        {
            throw new NotImplementedException();
        }

        private static ImagePtr calculateDifferenceImage8(ImagePtr imageA, ImagePtr imageB)
        {
            throw new NotImplementedException();
        }
    }
}
