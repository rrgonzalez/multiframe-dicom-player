﻿using Dicom;
using Dicom.Imaging;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

using DirtyRect;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Windows.Media;

namespace Multiframe_Dicom_Player
{
    public class PlayerController : BaseController, IPlayerController
    {
        private List<DicomImage> serie;
        private int[] frameTimeVector;
        private List< List<Int32Rect>> dirtyRectsSerie;
        private List<ImagePtr> serieImagePtr;
        private List<double> totalRepaintedPercent;
        private int _frameTimeSum;

        private List<MyDicom.DicomFile> serieMyDicom;

        public PlayerController()
        {
            serie = new List<DicomImage>();
            dirtyRectsSerie = new List< List<Int32Rect> >();
            serieImagePtr = new List<ImagePtr>();
            totalRepaintedPercent = new List<double>();

            ImageManager.SetImplementation(WPFImageManager.Instance);

            serieMyDicom = new List<MyDicom.DicomFile>();

        }

        public WriteableBitmap LoadDicomMYDICOM(string file)
        {
            var stream = new MemoryStream();
            MyDicom.DicomFile dicomDile = new MyDicom.DicomFile(file);
            dicomDile.Open(dicomDile.Name);

            Console.WriteLine(@"Image Loaded:");
            string patientName = dicomDile.DataSet.Get(MyDicom.VrDictionary.Tags.PatientsName).Get(0).ToString();
            Console.WriteLine(@"Patient Name: " + patientName);

            serieMyDicom.Clear();
            serieMyDicom.Add(dicomDile);

            int numberOfFrames = int.Parse(dicomDile.DataSet.Get(MyDicom.VrDictionary.Tags.NumberOfFrames).Get(0).ToString());
            if (numberOfFrames > 1)
            {
                frameTimeVector = new int[numberOfFrames];
                for (int i = 0; i < numberOfFrames; ++i) {
                    string time = dicomDile.DataSet.Get(MyDicom.VrDictionary.Tags.FrameTimeVectors).Get(i).ToString();
                    frameTimeVector[i] = int.Parse(time);
                }
            }

            // var df = (DeferrableStream)dicomDile.DataSet.Get(MyDicom.VrDictionary.Tags.PixelData).Get(0);

            _frameTimeSum = frameTimeVector[frameTimeVector.Length-1];
            foreach (int time in frameTimeVector)
                _frameTimeSum += time;

            //int totalDirtyArea;

            //int width = int.Parse( dicomDile.DataSet.Get(MyDicom.VrDictionary.Tags.NumberOfHorizontalPixels).Get(0).ToString() );
            //int heigth = int.Parse(dicomDile.DataSet.Get(MyDicom.VrDictionary.Tags.NumberOfVerticalPixels).Get(0).ToString());
            //int dpiX = 96;
            //int dpiY = 96;
            //var pixelData = (MyDicom.DeferredStream)dicomDile.DataSet.Get(MyDicom.VrDictionary.Tags.PixelData).Get(0);

            // Calculate the dirty Rects
            //if (numberOfFrames > 1)
            //{
            //    for (int i = 1; i < numberOfFrames; ++i)
            //    {
            //        totalDirtyArea = 0;

                    // Extract ImagePtr from DicomFile

                //    WriteableBitmap prevImage = serie[0].RenderImage(i - 1).AsWriteableBitmap();
                //    WriteableBitmap currentImage = serie[0].RenderImage(i).AsWriteableBitmap();

                //    serieImagePtr.Add(ImagePtr.FromBitmap(prevImage));

                //    List<Int32Rect> aux = DirtyRectLib.getDirtyRect(prevImage, currentImage);
                //    dirtyRectsSerie.Add(aux);

                //    foreach (Int32Rect dirtyRect in aux)
                //        totalDirtyArea += (dirtyRect.Width * dirtyRect.Height);
                //    totalRepaintedPercent.Add(totalDirtyArea * 100.0 / (image.Width * image.Height));
                //}

                //// Last and First image, to restart cycle
                //WriteableBitmap last = serie[0].RenderImage(image.NumberOfFrames - 1).AsWriteableBitmap();
                //WriteableBitmap first = serie[0].RenderImage(0).AsWriteableBitmap();
                //List<Int32Rect> restartList = DirtyRectLib.getDirtyRect(last, first);
                //dirtyRectsSerie.Add(restartList);
                //serieImagePtr.Add(ImagePtr.FromBitmap(last));

                //totalDirtyArea = 0;
                //foreach (Int32Rect dirtyRect in restartList)
                //    totalDirtyArea += (dirtyRect.Width * dirtyRect.Height);
                //totalRepaintedPercent.Add(totalDirtyArea * 100.0 / (image.Width * image.Height));
            //}

            return null;
        }

        public WriteableBitmap LoadDicom(string path)
        {
            var sourceFile = DicomFile.Open(path);
            var image = new DicomImage(sourceFile.Dataset);

            //Console.WriteLine(@"Image Loaded:");
            //string patientName = image.Dataset.Get<string>(DicomTag.PatientName);
            //Console.WriteLine(@"Patient Name: " + patientName);

            serie.Clear();
            serie.Add(image);

            //if (image.NumberOfFrames > 1)
            //    frameTimeVector = image.Dataset.Get<int[]>(DicomTag.FrameTimeVector);

            //_frameTimeSum = 0;
            //foreach (int time in frameTimeVector)
            //    _frameTimeSum += time;

            LoadDicomMYDICOM(path);

            int totalDirtyArea;

            // Calculate the dirty Rects
            if (image.NumberOfFrames > 1) {
                for (int i = 1; i < image.NumberOfFrames; ++i) {
                    totalDirtyArea = 0;

                    ImagePtr prevImage = ImagePtr.FromBitmap( RenderImage(i-1) );
                    ImagePtr currentImage = ImagePtr.FromBitmap( RenderImage(i) );

                    serieImagePtr.Add(prevImage);

                    List<Int32Rect> aux = DirtyRectLib.getDirtyRect(prevImage, currentImage);
                    dirtyRectsSerie.Add(aux);

                    foreach (Int32Rect dirtyRect in aux)
                        totalDirtyArea += (dirtyRect.Width * dirtyRect.Height);
                    totalRepaintedPercent.Add(totalDirtyArea *100.0 / (image.Width * image.Height));
                }

                // Last and First image, to restart cycle
                ImagePtr last = ImagePtr.FromBitmap( RenderImage(image.NumberOfFrames - 1) );
                ImagePtr first = ImagePtr.FromBitmap( RenderImage(0) );
                List<Int32Rect> restartList = DirtyRectLib.getDirtyRect(last, first);
                dirtyRectsSerie.Add(restartList);
                serieImagePtr.Add(last);

                totalDirtyArea = 0;
                foreach (Int32Rect dirtyRect in restartList)
                    totalDirtyArea += (dirtyRect.Width * dirtyRect.Height);
                totalRepaintedPercent.Add(totalDirtyArea*100.0 / (image.Width * image.Height));
            }

            return RenderImage();
        }

        public WriteableBitmap RenderImage(int index = 0)
        {
            return serie[0].RenderImage(index).AsWriteableBitmap();
        }

        public int NumberOfFrames()
        {
            return serie[0].NumberOfFrames;
        }

        public int GetFrameTime(int frame)
        {
            return frameTimeVector[frame];
        }

        public int TotalFrameTimeSum()
        {
            return _frameTimeSum;
        }

        public void RefreshCurrentFrame(WriteableBitmap renderedDicom, int currentFrame)
        {
            int prev = currentFrame - 1;
            if (currentFrame == 0)
                prev = serie.Count - 1;

            ImagePtr currentImagePtr = serieImagePtr[currentFrame];

            List<Int32Rect> dirtyRects = dirtyRectsSerie[prev];

            renderedDicom.Lock();

                unsafe
                {
                    var source = (byte*)currentImagePtr.Data;
                    var dest = (byte*)renderedDicom.BackBuffer;

                    Parallel.For(0, currentImagePtr.DataSize, i =>
                    {
                        dest[i] = source[i];
                    });
                }

                int index = 0;
                try
                {
                    foreach (Int32Rect rect in dirtyRects)
                    {
                        renderedDicom.AddDirtyRect(rect);
                        ++index;
                    }
                } catch (Exception e)
                {
                    Int32Rect rect = dirtyRects[index];
                    Console.WriteLine("Exception thrown on Rect: ", e.Message);
                    Console.WriteLine("TOPLEFT [" + rect.X + ", " + rect.Y + "]");
                    Console.WriteLine("RIGHTBOTTOM [" + (rect.X + rect.Height)  + ", " + (rect.Y + rect.Width) + "]");
                }

            renderedDicom.Unlock();

            Console.WriteLine("Repainted Percent: " + totalRepaintedPercent[currentFrame] + "%");
            Console.WriteLine("NON-Painted Pixels: " + (100.0 - totalRepaintedPercent[currentFrame]) + "%\n");
        }
    }
}
