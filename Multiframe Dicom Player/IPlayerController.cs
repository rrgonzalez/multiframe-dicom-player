﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Multiframe_Dicom_Player
{
    public interface IPlayerController : IController
    {
        WriteableBitmap LoadDicom(string path);
        WriteableBitmap RenderImage(int index);
        int NumberOfFrames();
        int GetFrameTime(int frame);
        int TotalFrameTimeSum();
        void RefreshCurrentFrame(WriteableBitmap renderedDicom, int currentFrame);
    }
}
