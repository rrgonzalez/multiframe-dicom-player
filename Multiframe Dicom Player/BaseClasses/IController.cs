﻿namespace Multiframe_Dicom_Player
{
    public interface IController
    {
        IController ParentController { get; set; }
    }
}