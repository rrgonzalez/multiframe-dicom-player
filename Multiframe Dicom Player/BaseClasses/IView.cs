﻿namespace Multiframe_Dicom_Player
{
    public interface IView
    {
        void ViewModelClosingHandler(bool? dialogResult);
        void ViewModelActivatingHandler();
        object DataContext { get; set; }
    }
}