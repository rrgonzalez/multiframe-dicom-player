﻿﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Multiframe_Dicom_Player
{
    /// <summary>
    /// When the VM is closed, the associated V needs to close too
    /// </summary>
    public delegate void ViewModelClosingEventHandler(bool? dialogResult);

    /// <summary>
    /// When a pre-existing VM is activated the View needs to activate itself
    /// </summary>
    public delegate void ViewModelActivatingEventHandler();

    /// <summary>
    /// A base class for all view models
    /// </summary>
    public abstract class BaseViewModel : ObservableObject
    {
        public event ViewModelClosingEventHandler ViewModelClosing;
        public event ViewModelActivatingEventHandler ViewModelActivating;

        #region Datagrid Editing

        private bool _datagridReadOnly = true;
        public bool DatagridReadOnly
        {
            get { return _datagridReadOnly; }
            set
            {
                _datagridReadOnly = value;
                RaisePropertyChanged("DatagridReadOnly");
            }
        }

        private RelayCommand _editRowCommand;
        public RelayCommand EditRowCommand
        {
            get
            {
                return _editRowCommand ??
                    (_editRowCommand = new RelayCommand(EditRowCommandExecuted));
            }
        }

        private void EditRowCommandExecuted()
        {
            DatagridReadOnly = false;
        }

        protected bool CheckSelectedNull(BaseViewData data, String name)
        {
            if (data == null)
            {
                MessageBox.Show(@"Por favor, antes seleccione el " + name + " a modificar", @"Seleccione el " + name);
                return false;
            }
            return true;
        }

        protected void ReportSavedChanges()
        {
            MessageBox.Show("Los cambios han sido guardados.", "Cambios guardados");
        }

        #endregion

        /// <summary>
        /// Keep a list of any children ViewModels so we can safely
        /// remove them when this ViewModel gets closed
        /// </summary>
        private readonly List<BaseViewModel> _childViewModels = new List<BaseViewModel>();
        public List<BaseViewModel> ChildViewModels
        {
            get { return _childViewModels; }
        }

        #region Bindable Properties

        #region ViewData
        private BaseViewData _viewData;
        public BaseViewData ViewData
        {
            get
            {
                return _viewData;
            }
            set
            {
                if (value != _viewData)
                {
                    _viewData = value;
                    RaisePropertyChanged("ViewData");
                }
            }
        }
        #endregion

        #endregion

        #region Controller

        /// <summary>
        /// If the ViewModel wants to do anything, it needs a controller
        /// </summary>
        protected IController Controller
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Parameterless Constructor required for support of DesignTime
        /// versions of View Models
        /// </summary>
        protected BaseViewModel()
        {
        }

        /// <summary>
        /// A view model needs a controller reference
        /// </summary>
        /// <param name="controller"></param>
        protected BaseViewModel(IController controller)
        {
            Controller = controller;
        }

        /// <summary>
        /// Create the View Model with a Controller and a FrameworkElement (View) injected.
        /// Note that we do not keep a reference to the View -
        /// just set its data context and
        /// subscribe it to our Activating and Closing events...
        /// Of course, this means there are references -
        /// that must be removed when the view closes,
        /// which is handled in the BaseView
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="contentView"></param>
        //public BaseViewModel(IController controller, FrameworkElement view)
        protected BaseViewModel(IController controller, IView contentView)
        : this(controller)
        {
            if (contentView == null) return;
            contentView.DataContext = this;
            ViewModelClosing += contentView.ViewModelClosingHandler;
            ViewModelActivating += contentView.ViewModelActivatingHandler;
        }

        #endregion

        #region public methods

        /// <summary>
        /// De-Register the VM from the Messenger to avoid non-garbage
        /// collected VMs receiving messages
        /// Tell the View (via the ViewModelClosing event) that we are closing.
        /// </summary>
        public virtual void CloseViewModel(bool? dialogResult)
        {
            ViewModelClosing?.Invoke(dialogResult);

            foreach (var childViewModel in _childViewModels)
            {
                childViewModel.CloseViewModel(dialogResult);
            }
        }

        public void ActivateViewModel()
        {
            ViewModelActivating?.Invoke();
        }

        #endregion

        #region Commands
        #region Commands Declaration

        private RelayCommand _closeAppCommand;
        public RelayCommand CloseAppCommand
        {
            get
            {
                return _closeAppCommand ??
                    (_closeAppCommand = new RelayCommand(CloseAppCommandExecuted));
            }
        }

        #endregion

        #region Command Handlers

        private void CloseAppCommandExecuted()
        {
            Application.Current.Shutdown(0);
        }

        #endregion
        #endregion
    }
}
