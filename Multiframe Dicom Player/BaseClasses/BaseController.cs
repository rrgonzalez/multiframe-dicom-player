﻿using System;

namespace Multiframe_Dicom_Player
{
    /// <summary>
    /// The base controller class.
    /// </summary>
    public abstract class BaseController : IController
    {
        public IController ParentController { get; set; }

        protected BaseController(BaseController parentController = null)
        {
            ParentController = parentController;
        }

        protected static void CheckNullArgument(object argument, string argumentName)
        {
            if (argument == null)
                throw new ArgumentNullException(argumentName);
        }
    }
}
