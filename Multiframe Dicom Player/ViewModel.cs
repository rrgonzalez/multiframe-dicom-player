﻿using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Multiframe_Dicom_Player
{
    public class ViewModel : BaseViewModel
    {
        public IPlayerController PlayerController => (IPlayerController) Controller;

        #region Constructor

        public ViewModel()
        {
            Controller = new PlayerController();
            PatientName = @"Chavela Vargas";
            PlayPauseButtonImageUrl = "assets/play.png";
            Playing = false;

            sw = new Stopwatch();
            controlReproductionTime = new Stopwatch();
        }

        #endregion

        #region Properties

        Stopwatch sw;
        Stopwatch controlReproductionTime;

        #region ObservableProperties

        private string _patientName;
        private string _dicomFilePath;
        private WriteableBitmap _renderedDicom;

        private bool _playing;
        private string _playPauseButtonImageUrl;
        private int _currentFrame;

        public string DicomFilePath
        {
            get { return _dicomFilePath; }
            set
            {
                _dicomFilePath = value;
                RaisePropertyChanged("DicomFilePath");
            }
        }

        public string PatientName
        {
            get { return _patientName; }
            set
            {
                if (value == _patientName) return;
                _patientName = value;
                RaisePropertyChanged("PatientName");
            }
        }

        public WriteableBitmap RenderedDicom
        {
            get { return _renderedDicom; }
            set
            {
                if (value == _renderedDicom) return;
                _renderedDicom = value;
                RaisePropertyChanged("RenderedDicom");
            }
        }

        public bool Playing
        {
            get { return _playing; }
            set
            {
                if (value == _playing) return;
                _playing = value;
                RaisePropertyChanged("Playing");

                if (value == true)
                {
                    PlayMultiframeImage();
                    PlayPauseButtonImageUrl = "assets/pause.png";
                } else
                {
                    PlayPauseButtonImageUrl = "assets/play.png";
                }
            }
        }

        public string PlayPauseButtonImageUrl
        {
            get => _playPauseButtonImageUrl;
            private set {
                if (value == _playPauseButtonImageUrl) return;
                _playPauseButtonImageUrl = value;
                RaisePropertyChanged("PlayPauseButtonImageUrl");
            }
        }

        public int CurrentFrame
        {
            get { return _currentFrame; }
            set
            {
                if (value == _currentFrame) return;
                _currentFrame = value;
                RaisePropertyChanged("CurrentFrame");
            }
        }

        #endregion
        #endregion

        #region Commands

        private ICommand _selectDicomFileCommand;
        private ICommand _togglePlayPauseCommand;

        public ICommand SelectDicomFileCommand => _selectDicomFileCommand ??
                                                    (_selectDicomFileCommand = new RelayCommand(SelectDicomFileExecuted));
        public ICommand TogglePlayPauseCommand => _togglePlayPauseCommand ??
                                                    (_togglePlayPauseCommand = new RelayCommand(_togglePlayPauseExecuted));

        #region Command Executed

        private void SelectDicomFileExecuted()
        {
            var openFileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = false
            };
            
            var result = openFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            DicomFilePath = openFileDialog.FileName;
            
            RenderedDicom = PlayerController.LoadDicom(DicomFilePath);
            CurrentFrame = 0;
        }
        private void _togglePlayPauseExecuted()
        {
            if (PlayerController.NumberOfFrames() == 1)
                return;

            Playing = !Playing;
        }

        #endregion
        #endregion

        #region Methods
        private async Task PlayMultiframeImage()
        {
            sw.Reset();
            while (Playing)
            {
                if (CurrentFrame == 0)
                {
                    if (sw.IsRunning)
                    { 
                        sw.Restart();
                    }
                    else
                    {
                        sw.Start();
                    }
                } else if (CurrentFrame == PlayerController.NumberOfFrames() - 1)
                {
                    System.Console.WriteLine("Sum of Frametimes: {0}", PlayerController.TotalFrameTimeSum());
                    System.Console.WriteLine("Current cycle reproduction time: {0}", sw.ElapsedMilliseconds);
                }

                if (CurrentFrame + 1 < PlayerController.NumberOfFrames())
                    CurrentFrame++;
                else
                    CurrentFrame = 0;

                Task delay = Task.Delay(PlayerController.GetFrameTime(CurrentFrame) >> 3);
                
                // RenderedDicom = PlayerController.RenderImage(_currentFrame);
                PlayerController.RefreshCurrentFrame(RenderedDicom, CurrentFrame);

                await delay;
            }

            sw.Stop();
        }

        #endregion
    }
}
